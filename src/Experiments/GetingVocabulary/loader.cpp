#include "loader.h"
#include <fstream>
#include <stdlib.h>    
#include <time.h> 

/*********************************************************************
 * random_generator */
#define generator_lag_a 97
#define generator_lag_b 33
#define generator_lag_max 97
pthread_mutex_t random_generator::rand_lock=PTHREAD_MUTEX_INITIALIZER;


random_generator::random_generator()
	{
	mas=new double[generator_lag_max];
	id=0;
	pthread_mutex_lock(&rand_lock);
	srand (time(NULL));
	for(unsigned char perc=0;perc<generator_lag_max;perc++)
		mas[perc]=double(rand())/double(RAND_MAX);
	pthread_mutex_unlock(&rand_lock);
	}
random_generator::~random_generator()
	{
	delete mas;
	}
double random_generator::operator ()()
	{
	if(id<generator_lag_b)
		per_id=generator_lag_a-generator_lag_b+id;
	else
		per_id=id-generator_lag_b;
	if(mas[id]>=mas[per_id])
		mas[id]-=mas[per_id];
	else
		mas[id]-=mas[per_id]-1;
	if(id==generator_lag_max-1)
		{
		id=0;
		return mas[generator_lag_max-1];
		}
	else
		{
		id++;
		return mas[id-1];
		}
	}

/*********************************************************************
 * proxy_provider */

proxy_provider::proxy_object::proxy_object()
	{
	object=NULL;
	}

proxy_provider::proxy_object::proxy_object(const proxy_object &in)
	{
	parent=in.parent;
	object=in.object;
	}

proxy_provider::proxy_object::~proxy_object()
	{
	}

unsigned short proxy_provider::proxy_object::port()
	{
	if(object!=NULL)
		return object->port;
	else
		return 0;
	}

boost::asio::ip::address_v4 proxy_provider::proxy_object::ip()
	{
	if(object!=NULL)
		return object->ip;
	else
		return boost::asio::ip::address_v4::from_string("0.0.0.0");
	}

void proxy_provider::proxy_object::renew()
	{
	double r_number=parent->sum*rand();
	for(auto it=parent->proxies.begin();it!=parent->proxies.end();++it)
		{
		r_number-=it->weight;
		if(r_number<0)
			{
			object=&(*it);
			break;
			}
		}
	}

void proxy_provider::proxy_object::bad()
	{
	if(object!=NULL)
		{
		object->weight/=2.0;
		parent->sum-=object->weight;
		}
	}

void proxy_provider::load_list_form_file(std::ifstream &in)
	{
	int col=0;
	char ch;
	boost::asio::ip::address_v4::bytes_type ip;
	int perc;
	proxy per_proxy;
	per_proxy.weight=1.0;
	in.get(ch);
	while(in)
		{
		for(perc=0;perc<4;perc++)
			{
			ip[perc]=0;
			while(1)
				{
				if(ch=='.'||ch==':')
					break;
				ip[perc]=ip[perc]*10+ch-48;
				in.get(ch);
				}
			in.get(ch);
			}
		per_proxy.ip=boost::asio::ip::address_v4(ip);
		per_proxy.port=0;
		while(1)
			{
			if(ch=='\n')
				break;
			per_proxy.port=per_proxy.port*10+ch-48;
			in.get(ch);
			}
		sum+=1.0;
		proxies.push_back(per_proxy);
		col++;
		in.get(ch);
		}
	std::cout<<"Added "<<col<<" proxies"<<std::endl;
	}

proxy_provider::proxy_provider()
	{
	sum=0;
	}

proxy_provider::proxy_provider(const std::string &file_name)
	{
	sum=0;
	std::cout<<"Reading proxy list...."<<std::endl;
	std::ifstream input_file(file_name,std::ifstream::in);
	if(input_file.is_open()==false)
		{
		std::cout<<"Fail to open file: "<<file_name<<std::endl;
		std::cout<<"Proxy list don't load"<<std::endl;
		return;
		}
	load_list_form_file(input_file);
	input_file.close();
	std::cout<<"Proxies are loaded"<<std::endl;
	}

proxy_provider::~proxy_provider()
	{
	}
		
void proxy_provider::add_proxy(const boost::asio::ip::address_v4 &ip,const unsigned short &port)
	{
	}
		
void proxy_provider::proxies_from_file(const std::string &file_name)
	{
	std::cout<<"Reading proxy list...."<<std::endl;
	std::ifstream input_file(file_name,std::ifstream::in);
	if(input_file.is_open()==false)
		{
		std::cout<<"Fail to open file: "<<file_name<<std::endl;
		std::cout<<"Proxy list don't load"<<std::endl;
		return;
		}
	load_list_form_file(input_file);
	input_file.close();
	std::cout<<"Proxies are loaded"<<std::endl;
	}

proxy_provider::proxy_object proxy_provider::operator() (void)
	{
	proxy_object per;
	per.parent=this;
	per.renew();
	return per;
	}
/*********************************************************************
 * job_provider */
