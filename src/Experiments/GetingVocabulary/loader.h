#ifndef  __loader_h_
#define  __loader_h_
#include <boost/asio.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <pthread.h>

class random_generator
	{
	private:
		static pthread_mutex_t rand_lock;
		double *mas;
		unsigned char id;
		unsigned char per_id;
	public:
		random_generator();
		~random_generator();
		double operator() (void);
	};


class proxy_provider
	{
	private:
		struct proxy
			{
			unsigned short port;
			boost::asio::ip::address_v4 ip;
			double weight;
			};
	public:
		class proxy_object
			{
			private:
				friend proxy_provider;
				proxy_provider *parent;
				proxy *object;
				random_generator rand;
				proxy_object();
			public:
				proxy_object(const proxy_object&);
				~proxy_object();
				unsigned short port();
				boost::asio::ip::address_v4 ip();
				void renew();
				void bad();
			};
	private:
		std::vector<proxy > proxies;
		double sum;
		void load_list_form_file(std::ifstream &);
	public:
		proxy_provider();
		proxy_provider(const std::string &);
		~proxy_provider();
		void proxies_from_file(const std::string &);
		void add_proxy(const boost::asio::ip::address_v4 &,const unsigned short &);
		proxy_object operator() (void);
	};
#endif
