#include <iostream>
#include <chrono>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include "loader.h"



int main(int argc,char** argv)
	{
	try
        {
        boost::program_options::options_description options("Available options");
        options.add_options()
            ("help,h", "Show help")
            ("proxy_input", boost::program_options::value<std::string>(), "File with list of proxy servers .txt")
            ;
        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_command_line(argc,argv,options), vm);
        
        if(vm.count("help"))
            {
            std::cout << options << std::endl;
            return 1;
            }
        
        proxy_provider proxies;
        if (vm.count("proxy_input")) 
            {
            proxies.proxies_from_file(vm["proxy_input"].as<std::string>());
            }
       proxy_provider::proxy_object po=proxies();
       std::cout<<"ip:"<<po.ip()<<" port:"<<po.port()<<std::endl;
       /*else if(vm.count("help"))
            {
            std::cout << options << std::endl;
            }*/
        /*random_generator per;
        double perd,perd1;
        int id;
        int mas[100];
        for(int k=0;k<100;k++)
            mas[k]=0;
        auto t_start = std::chrono::high_resolution_clock::now();
        for(int j=0;j<100000000;j++)
            {
            perd=per.get_value();
            id=0;
            while(1)
                {
                perd1=0.01*double(id+1);
                if(perd1>perd)
                    break;
                id++;
                }
            mas[id]++;
            }
        auto t_end = std::chrono::high_resolution_clock::now();
        std::cout<<std::chrono::duration<double, std::milli>(t_end-t_start).count()<<std::endl;
        for(int k=0;k<100;k++)
            std::cout<<mas[k]/1000000.0<<std::endl;*/
        
        /*boost::asio::io_service io_service;
        boost::asio::ip::tcp::resolver resolver(io_service);
        boost::asio::ip::tcp::resolver::query query("www.vedu.ru", "http");
        boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
        boost::asio::ip::tcp::socket socket(io_service), socket1(io_service);
        boost::asio::ip::tcp::socket::keep_alive ka(true);
        boost::asio::ip::tcp::endpoint ep3( boost::asio::ip::address::from_string("221.176.14.72"), 80);
        boost::asio::connect(socket, &ep3);
        socket.set_option(ka);
        boost::asio::streambuf request, request1;
        std::ostream request_stream(&request),request_stream1(&request1);
        request_stream << "GET " << "http://www.vedu.ru/expdic/5/" << " HTTP/1.1\r\n";
        request_stream << "Host: " << "www.vedu.ru" << "\r\n";
        request_stream << "Accept: * / *\r\n";
        request_stream << "Connection: Keep-Alive\r\n\r\n";
        boost::asio::write(socket, request);
        //std::cout<<&request;
        boost::asio::streambuf response,response1;
        boost::asio::read_until(socket, response, "\r\n");
        std::istream response_stream(&response);
        std::string http_version;
        response_stream >> http_version;
        unsigned int status_code;
        response_stream >> status_code;
        std::string status_message;
        std::getline(response_stream, status_message);
        if (!response_stream || http_version.substr(0, 5) != "HTTP/")
            {
            std::cout << "Invalid response\n";
            return 1;
            }
        if (status_code != 200)
            {
            std::cout << "Response returned with status code " << status_code << "\n";
            return 1;
            }
        boost::asio::read_until(socket, response, "\r\n\r\n");
        std::string header;
        while (std::getline(response_stream, header) && header != "\r")
            std::cout << header << "\n";
        std::cout << "\n";
        if (response.size() > 0)
            std::cout << &response;
        boost::system::error_code error;
        std::ostringstream stream,stream1;
        while (boost::asio::read(socket, response,
            boost::asio::transfer_at_least(1), error))
        stream << &response;
        std::string str =  stream.str();
        std::cout <<str<< std::endl;
        if (error != boost::asio::error::eof)
            throw boost::system::system_error(error);
        std::cout<<"**********************************************"<<std::endl;
        //std::cout<<&request;
        //socket.close();
        //boost::asio::connect(socket, &ep3);
        request_stream1 << "GET " << "http://www.vedu.ru/expdic/70/" << " HTTP/1.1\r\n";
        request_stream1 << "Host: " << "www.vedu.ru" << "\r\n";
        request_stream1 << "Accept: * / *\r\n";
        request_stream1 << "Connection: close\r\n\r\n";
        boost::asio::write(socket, request1);
        while (boost::asio::read(socket, response1,
            boost::asio::transfer_at_least(1), error))
        stream1 << &response1;
        std::string str1 =  stream1.str();
        std::cout <<str1<< std::endl;
        if (error != boost::asio::error::eof)
            throw boost::system::system_error(error);
        //*/
        }
	catch (std::exception& e)
        {
        std::cout << "Exception: " << e.what() << "\n";
        }
    return 0;
	}
